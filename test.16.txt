# Title: test.16
# Author: Derek Neil
# Purpose: JMP operations test
# How test works: 
#	load char into register 1
#	call method to out char on own line
#	jump to end of program pointer
# Exptected result: 
#	register output should match test.15.out
#	switching back and forth between std and cld
#	if call worked there should be 5 on it's own line
#	then more register print outs
#	if jmp worked, there should be no more output