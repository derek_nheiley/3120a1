#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define X_INSTRUCTIONS_NOT_NEEDED

#include "xis.h"
#include "xcpu.h"

/* Use 
 *   xcpu_print( c );
 * to print cpu state, where c is of type xcpu * 
 * See xcpu.h for full prototype of xcpu_print()
 */

extern int xcpu_execute( xcpu *c ) {

	//fetch current ######## instruction
	unsigned short instruction = *(c->memory+c->pc);
	c->pc++; //increment program counter
	
	//decode instruction for operators and immediate required
	unsigned short bit1_2 = instruction >> 6; // ##______
	unsigned short op1=0, op2=0, immediate=0;

	if( bit1_2 > 0 ){ 
		unsigned short bit3 = instruction>>5 & 1;
		unsigned short operators= *(c->memory+c->pc);
		if ( bit1_2 == 1 ){
			if ( bit3 ){ 	// 011##### IIIIIIII
				immediate = *(c->memory+c->pc);
			}
			else{ // 010##### RRRR____
				op1 = operators >> 4;
			}
		}
		else if ( bit1_2 == 2 ){ // 10###### SSSSDDDD
			op1 = operators >> 4;
			op2 = operators & 0xf;
		}
		else if ( bit1_2 == 3 ){
			if( bit3 ){ // 111##### RRRR____  IIIIIIII IIIIIIII
				op1 = operators >> 4;
			}
			// 110##### ________  IIIIIIII IIIIIIII
			c->pc++;
			immediate = *(c->memory+c->pc) << 8;
			c->pc++;
			immediate = immediate | *(c->memory+c->pc);
		}	
	}
	c->pc++;

	//decode actual instruction, execute relevant code
	switch (instruction){
		case I_BAD :
			return 0;
		case I_RET  :
			c->pc = *(c->memory+c->regs[15]+1)
					 | (*(c->memory+c->regs[15]))<<8;
			c->regs[15] = c->regs[15] + 2;
			break;
		case I_CLD  :
			c->state = c->state & 0xFFFD;
			break;
		case I_STD  :
			c->state = c->state | 2;
			break;
		case I_NEG  :
			c->regs[op1] = (~c->regs[op1] & 0xFFFF )+1;
			break;
		case I_NOT  :
			c->regs[op1] = !c->regs[op1];
			break;
		case I_PUSH :
			c->regs[15] = c->regs[15] - 2;
			*(c->memory+c->regs[15]+1) = c->regs[op1];
			*(c->memory+c->regs[15]) = c->regs[op1]>>8;
			break;
		case I_POP  :
			c->regs[op1] = *(c->memory+c->regs[15]+1)
							 | (*(c->memory+c->regs[15]))<<8;
			c->regs[15] = c->regs[15] + 2;
			break;
		case I_JMPR :
			c->pc = c->regs[op1];
			break;
		case I_CALLR:
			c->regs[15] = c->regs[15] - 2;
			*(c->memory+c->regs[15]+1) = c->pc;
			*(c->memory+c->regs[15]) = c->pc>>8;
			c->pc = c->regs[op1];
			break;
		case I_OUT  :
			printf("%c", c->regs[op1]);
			break;
		case I_INC  :
			c->regs[op1] = c->regs[op1] + 1;
			break;
		case I_DEC  :
			c->regs[op1] = c->regs[op1] - 1;
			break;
		case I_BR   :
			if( (c->state & 1) == 1 ){
				if( ((immediate>>4) & 0xF) == 0xF ){
					immediate = ( ~immediate & 0xF ) + 1;
					c->pc = c->pc-2 - immediate;
				}
				else {
					c->pc = c->pc-2 + immediate;
				}
			}
			break;
		case I_JR   :
			if( ((immediate>>4) & 0xF) == 0xF ){
				immediate = ( ~immediate & 0xF ) + 1;
				c->pc = c->pc-2 - immediate;
			}
			else {
				c->pc = c->pc-2 + immediate;
			}
			break;
		case I_ADD  :
			c->regs[op2] = c->regs[op2] + c->regs[op1];
			break;
		case I_SUB  :
			c->regs[op2] = c->regs[op2] - c->regs[op1];
			break;
		case I_MUL  :
			c->regs[op2] = c->regs[op2] * c->regs[op1];
			break;
		case I_DIV  :
			c->regs[op2] = c->regs[op2] / c->regs[op1];
			break;
		case I_AND  :
			c->regs[op2] = c->regs[op2] & c->regs[op1];
			break;
		case I_OR   :
			c->regs[op2] = c->regs[op2] | c->regs[op1];
			break;
		case I_XOR  :
			c->regs[op2] = c->regs[op2] ^ c->regs[op1];
			break;
		case I_SHR  :
			c->regs[op2] = c->regs[op2] >> c->regs[op1];
			break;
		case I_SHL  :
			c->regs[op2] = c->regs[op2] << c->regs[op1];
			break;
		case I_TEST :
			;
			unsigned short test = c->regs[op1] & c->regs[op2];
			if ( test!=0 )
				c->state = c->state | 1;
			else
				c->state = c->state & 0xFFFE;
			break;
		case I_CMP  :
			if ( c->regs[op1] < c->regs[op2] )
				c->state = c->state | 1;
			else
				c->state = c->state & 0xFFFE;
			break;
		case I_EQU  :
			if ( c->regs[op1] == c->regs[op2] )
				c->state = c->state | 1;
			else
				c->state = c->state & 0xFFFE;
			break;
		case I_MOV  :
			c->regs[op2] = c->regs[op1];
			break;
		case I_LOAD :
			c->regs[op2] = *(c->memory+c->regs[op1]+1) 
							| (*(c->memory+c->regs[op1]))<<8;
			break;
		case I_STOR :
			*(c->memory+c->regs[op2]+1) = c->regs[op1];
			*(c->memory+c->regs[op2]) = c->regs[op1]>>8;
			break;
		case I_LOADB: //(byte)
			c->regs[op2] = *(c->memory+c->regs[op1]);
			break;
		case I_STORB: //(byte)
			*(c->memory+c->regs[op2]) = c->regs[op1];
			break;
		case I_JMP  :
			c->pc = immediate;
			break;
		case I_CALL :
			c->regs[15] = c->regs[15] - 2;
			*(c->memory+c->regs[15]+1) = c->pc;
			*(c->memory+c->regs[15]) = c->pc>>8;
			c->pc = immediate;
			break;
		case I_LOADI:
			c->regs[op1] = immediate;
			break;

		default:
			return 0; //illegal instruction was executed
	}
	unsigned short print = c->state & 2;
	if( print ) xcpu_print( c );
	return 1; //instruction was successfully executed
}


/* Not needed for assignment 1 */
int xcpu_exception( xcpu *c, unsigned int ex ) {
  return 0;
}
