# Title: test.11
# Author: Derek Neil
# Purpose: NOT and INC operations test
# How test works: 
#	load char into register 0
#	call method to output char on own line
#	test incr by incrementing to next char
#	output again by calling printr1 method
#	not char and print out register to check result
# Exptected result: 
#	register output should match test.11.out
#	switching back and forth between std and cld
#	there should be ! on it's own line, more register print outs
#	then " on it's own line since it's after ! in the hex char map
#	then r1 should zero out as the logical negation of anything
#	other than zero