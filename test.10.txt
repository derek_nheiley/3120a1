# Title: test.10
# Author: Derek Neil
# Purpose: STORB and NEG operations test
# How test works: 
#	load char into register 0
#	store char to stack manually manipulating stack pointer
#	use storb to load only 0x21 into the stack
#	use full LOAD to get anything that was stored in the stack
#	output, then negate r1
# Exptected result: 
#	register output should match test.10.out
#	there should be a ! followed by two more register outputs
#	register 1 should contain 0xffdf if it worked
#	two's compliment of 0x0021