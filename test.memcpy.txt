# Title: test.memcpy
# Author: Derek Neil
# Purpose: make sure memcpy is working
# How test works: 
#	store two chars in the stack (one word each)
#	save this point on the stack as the source
#	push two underscores chars onto the stack (one word each)
#	save this point on the stack as the dest (to replace the _'s)
#	save 4 bytes (2 8-bit words on our 16 bit machine) as the length
#	call memcpy
#	and pop input params off stack to get rid of them
#	pop the 2 destination words, and the 2 source words printing them to screen
# Exptected result: 
#	the output should read HiHi if memcpy worked
#	or the output might read __Hi if memcpy failed
