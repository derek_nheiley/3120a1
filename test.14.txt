# Title: test.14
# Author: Derek Neil
# Purpose: CALLR and OUT operations test
# How test works: 
#	load char into register 1
#	call method to out char on own line
#	callr by loading pointer to end of program to r2
#	jump to end of program pointer stored in r2
# Exptected result: 
#	register output should match test.14.out
#	switching back and forth between std and cld
#	there should be 4 on it's own line, more register print outs
#	if callr also worked the program should not out any more char 4's