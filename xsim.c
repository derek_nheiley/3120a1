#include "xcpu.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#define XMEMSIZE 65536 

int main(int argc, char *argv[]){

	if ( argc < 3 ){
    	return 1;
    }
    int maxCycles = strtol(argv[1], NULL, 10);

	//instantiate an xcpu
	xcpu *c;
	if((c = malloc(sizeof *c)) == NULL){
		return 1;
	}

  	//allocate 64KB, assign pointer to c.memory
	c->memory = malloc(XMEMSIZE);

	//load specified program into c.memory
   	FILE * fp = fopen(argv[2], "r");
   	if (fp == NULL){ 
   		return 1;
   	}

   	int count = 0;
   	while (!feof(fp) && count < XMEMSIZE) {
        count += fread((c->memory+count), 1, 1, fp);
    }

    //close file
	fclose(fp);

	//check if program was too big
	if (count == XMEMSIZE ){
   		return 1;
	}

	//zero registers
	int i;
	for(i=0; i<X_MAX_REGS; i++){
		c->regs[i]=0;
	}

	//zero Program Counter and other shorts
	c->pc = 0;
	c->state = 0;
	c->itr = 0;

	//identify and setup cpu
	c->id = 0;
	c->num = 1;

	if( maxCycles < 1 ){
		//program should never run out of cycles
		maxCycles = -1;
	}
	int noErrors = 1;
	while( noErrors==1 && maxCycles!=0 ){
		if( maxCycles > 0 ){
			maxCycles--;
		}
		noErrors = xcpu_execute( c );
	}

	if( maxCycles==0 ){
		printf("CPU ran out of time.\n");
		return 1;
	}

	if( noErrors==0 ){
		printf("CPU has halted.\n");
		return 1;
	} 
	return 0;
}

